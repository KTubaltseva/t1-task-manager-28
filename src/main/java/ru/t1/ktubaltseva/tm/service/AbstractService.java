package ru.t1.ktubaltseva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.repository.IRepository;
import ru.t1.ktubaltseva.tm.api.service.IService;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.IndexIncorrectException;
import ru.t1.ktubaltseva.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final R modelRepository;

    public AbstractService(@NotNull final R modelRepository) {
        this.modelRepository = modelRepository;
    }

    @NotNull
    @Override
    public M add(@Nullable final M model) throws EntityNotFoundException {
        if (model == null) throw new EntityNotFoundException();
        modelRepository.add(model);
        return model;
    }

    @NotNull
    @Override
    public Collection<M> add(@Nullable final Collection<M> models) {
        if (models == null || models.isEmpty()) return Collections.emptyList();
        return modelRepository.add(models);
    }

    @NotNull
    @Override
    public Collection<M> set(@Nullable final Collection<M> models) {
        if (models == null || models.isEmpty()) return Collections.emptyList();
        return modelRepository.set(models);
    }

    @Override
    public void clear() {
        modelRepository.clear();
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return modelRepository.findAll();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return modelRepository.findAll(comparator);
    }

    @NotNull
    @Override
    public M findOneById(@Nullable final String id) throws IdEmptyException, EntityNotFoundException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M model = modelRepository.findOneById(id);
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    @NotNull
    @Override
    public M findOneByIndex(@Nullable final Integer index) throws IndexIncorrectException, EntityNotFoundException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= modelRepository.getSize()) throw new IndexIncorrectException();
        @Nullable final M model = modelRepository.findOneByIndex(index);
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    @Override
    public void removeAll() {
        modelRepository.removeAll();
    }

    @NotNull
    @Override
    public M removeOne(@Nullable final M model) throws EntityNotFoundException, UserNotFoundException {
        if (model == null) throw new EntityNotFoundException();
        @Nullable final M modelRemoved = modelRepository.removeOne(model);
        if (modelRemoved == null) throw new EntityNotFoundException();
        return modelRemoved;
    }

    @NotNull
    @Override
    public M removeById(@Nullable final String id) throws IdEmptyException, EntityNotFoundException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M modelRemoved = modelRepository.removeById(id);
        if (modelRemoved == null) throw new EntityNotFoundException();
        return modelRemoved;
    }

    @NotNull
    @Override
    public M removeByIndex(@Nullable final Integer index) throws IndexIncorrectException, EntityNotFoundException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= modelRepository.getSize()) throw new IndexIncorrectException();
        @Nullable final M modelRemoved = modelRepository.removeByIndex(index);
        if (modelRemoved == null) throw new EntityNotFoundException();
        return modelRemoved;
    }

}
