package ru.t1.ktubaltseva.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.ktubaltseva.tm.api.repository.ICommandRepository;
import ru.t1.ktubaltseva.tm.api.repository.IProjectRepository;
import ru.t1.ktubaltseva.tm.api.repository.ITaskRepository;
import ru.t1.ktubaltseva.tm.api.repository.IUserRepository;
import ru.t1.ktubaltseva.tm.api.service.*;
import ru.t1.ktubaltseva.tm.command.AbstractCommand;
import ru.t1.ktubaltseva.tm.command.data.AbstractDataCommand;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.ktubaltseva.tm.exception.system.CommandNotSupportedException;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.Task;
import ru.t1.ktubaltseva.tm.model.User;
import ru.t1.ktubaltseva.tm.repository.CommandRepository;
import ru.t1.ktubaltseva.tm.repository.ProjectRepository;
import ru.t1.ktubaltseva.tm.repository.TaskRepository;
import ru.t1.ktubaltseva.tm.repository.UserRepository;
import ru.t1.ktubaltseva.tm.service.*;
import ru.t1.ktubaltseva.tm.util.SystemUtil;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.util.Set;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.ktubaltseva.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService(loggerService);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(
            userRepository,
            taskRepository,
            projectRepository,
            propertyService
    );

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes)
            registry(clazz);
    }

    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        try {
            Files.write(Paths.get(filename), pid.getBytes());
            @NotNull final File file = new File(filename);
            file.deleteOnExit();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initDemoData() throws AbstractException, NoSuchAlgorithmException {

        System.out.println("[INIT DEMO DATA]");

        @NotNull final User user1 = userService.create("USER1", "USER1");
        @NotNull final User user2 = userService.create("USER2", "USER2");
        @NotNull final User admin = userService.create("ADMIN", "ADMIN", Role.ADMIN);

        projectService.add(user1.getId(), new Project("pn2", "pd2", Status.IN_PROGRESS));
        projectService.add(user1.getId(), new Project("pn5", "pd5", Status.NOT_STARTED));
        projectService.add(user2.getId(), new Project("pn3", "pd3", Status.IN_PROGRESS));
        projectService.add(admin.getId(), new Project("pn4", "pd4", Status.COMPLETED));

        taskService.add(user1.getId(), new Task("tn2", "td2", Status.IN_PROGRESS));
        taskService.add(user1.getId(), new Task("tn5", "td5", Status.NOT_STARTED));
        taskService.add(user2.getId(), new Task("tn3", "td3", Status.IN_PROGRESS));
        taskService.add(admin.getId(), new Task("tn4", "td4", Status.COMPLETED));
    }

    private void initData() throws AbstractException, NoSuchAlgorithmException {
        final boolean checkBinary = Files.exists(Paths.get(AbstractDataCommand.FILE_BINARY));
        if (checkBinary) {
            processCommand("data-load-binary", false);
            return;
        }

        final boolean checkBase64 = Files.exists(Paths.get(AbstractDataCommand.FILE_BASE64));
        if (checkBase64) {
            processCommand("data-load-base64", false);
            return;
        }

        initDemoData();
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(() -> loggerService.info("** TASK MANAGER IS SHUTTING DOWN **")));
    }

    private boolean processArgument(@Nullable final String[] args) throws ArgumentNotSupportedException, CommandNotSupportedException {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        processArgument(arg);
        return true;
    }

    private void processArgument(@Nullable final String argument) throws ArgumentNotSupportedException, CommandNotSupportedException {
        if (argument == null) throw new ArgumentNotSupportedException();
        commandService.getCommandByArgument(argument);
    }

    private void processCommand(@Nullable final String command) throws AbstractException, NoSuchAlgorithmException {
        processCommand(command, true);
    }

    private void processCommand(@Nullable final String command, final boolean checkRoles) throws AbstractException, NoSuchAlgorithmException {
        if (command == null) throw new CommandNotSupportedException();
        @NotNull final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (checkRoles) getAuthService().checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NotNull final AbstractCommand command = clazz.newInstance();
        registry(command);
    }


    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    public void run(@Nullable final String[] args) throws AbstractException, NoSuchAlgorithmException {
        BasicConfigurator.configure();

        initPID();

        if (processArgument(args)) System.exit(0);

        initData();
        initLogger();

        processCommands();
    }

}
