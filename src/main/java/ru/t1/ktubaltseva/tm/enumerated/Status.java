package ru.t1.ktubaltseva.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.exception.field.StatusIncorrectException;

@Getter
public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    @NotNull
    private final String displayName;

    Status(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public static String toName(@Nullable final Status status) {
        if (status == null) return "";
        return status.getDisplayName();
    }

    @NotNull
    public static Status toStatus(@Nullable final String value) throws StatusIncorrectException {
        if (value == null || value.isEmpty()) throw new StatusIncorrectException();
        for (@NotNull final Status status : values()) {
            if (status.name().equals(value)) return status;
        }
        throw new StatusIncorrectException();
    }

}
