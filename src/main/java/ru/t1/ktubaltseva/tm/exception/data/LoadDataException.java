package ru.t1.ktubaltseva.tm.exception.data;

public class LoadDataException extends AbstractDataException {

    public LoadDataException() {
        super("Error! Failed to load data...");
    }

}
