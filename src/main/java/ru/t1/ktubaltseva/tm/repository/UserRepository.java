package ru.t1.ktubaltseva.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.repository.IUserRepository;
import ru.t1.ktubaltseva.tm.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public @Nullable User findByLogin(@NotNull final String login) {
        return models
                .stream()
                .filter(m -> login.equals(m.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public @Nullable User findByEmail(@NotNull final String email) {
        return models
                .stream()
                .filter(m -> email.equals(m.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public Boolean isLoginExists(@NotNull final String login) {
        return models
                .stream()
                .anyMatch(m -> login.equals(m.getLogin()));
    }

    @NotNull
    @Override
    public Boolean isEmailExists(@NotNull final String email) {
        return models
                .stream()
                .anyMatch(m -> email.equals(m.getEmail()));
    }

}
