package ru.t1.ktubaltseva.tm.command.data;

import lombok.Cleanup;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.Domain;
import ru.t1.ktubaltseva.tm.exception.data.LoadDataException;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DataLoadBase64Command extends AbstractDataCommand {

    @NotNull
    private final String NAME = "data-load-base64";

    @NotNull
    private final String DESC = "Load data from base64 file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws LoadDataException {
        System.out.println("[LOAD BASE64 DATA]");
        try {
            @NotNull final byte[] base64Byte = Files.readAllBytes(Paths.get(FILE_BASE64));
            @Nullable final String base64Str = new String(base64Byte);
            @Nullable final byte[] bytes = new BASE64Decoder().decodeBuffer(base64Str);
            @Cleanup @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
            @Cleanup @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
            @NotNull final Domain domain = (Domain) objectInputStream.readObject();
            setDomain(domain);
        } catch (IOException | ClassNotFoundException e) {
            throw new LoadDataException();
        }
    }

}
