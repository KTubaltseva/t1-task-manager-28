package ru.t1.ktubaltseva.tm.command.data;

import lombok.Cleanup;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.dto.Domain;
import ru.t1.ktubaltseva.tm.exception.data.LoadDataException;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DataLoadBinaryCommand extends AbstractDataCommand {

    @NotNull
    private final String NAME = "data-load-binary";

    @NotNull
    private final String DESC = "Load data from binary file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws LoadDataException {
        System.out.println("[LOAD BINARY DATA]");
        try {
            @Cleanup @NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_BINARY);
            @Cleanup @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            @NotNull final Domain domain = (Domain) objectInputStream.readObject();
            setDomain(domain);
        } catch (IOException | ClassNotFoundException e) {
            throw new LoadDataException();
        }
    }

}
