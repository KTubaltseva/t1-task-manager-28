package ru.t1.ktubaltseva.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.Cleanup;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.dto.Domain;
import ru.t1.ktubaltseva.tm.exception.data.SaveDataException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class DataSaveXmlFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private final String NAME = "data-save-xml-faster-xml";

    @NotNull
    private final String DESC = "Save data to xml file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws SaveDataException {
        System.out.println("[SAVE XML DATA]");
        try {
            @NotNull final Domain domain = getDomain();
            @NotNull final File file = new File(FILE_XML);
            @NotNull final Path path = file.toPath();

            Files.deleteIfExists(path);
            Files.createFile(path);

            @Cleanup @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
            @NotNull final ObjectMapper objectMapper = new XmlMapper();
            @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            fileOutputStream.write(xml.getBytes());
            fileOutputStream.flush();
        } catch (IOException e) {
            throw new SaveDataException();
        }
    }

}
